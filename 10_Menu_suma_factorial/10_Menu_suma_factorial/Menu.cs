﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _10_Menu_suma_factorial
{
    class Menu
    {
        public Menu() {
        }
        public void resolver(int opcion) {
            
            Menu msf = new Menu();
            Program principal = new Program();
            Console.WriteLine($"la opcion es {opcion}");
            switch (opcion)                 
            {
                case 1:
                    Console.WriteLine($"Escogió la opcion # {opcion} pulse cualquier tecla para salir.");
                    Console.ReadKey();
                    break;

                case 2:
                    
                    int suma =Convert.ToInt32(msf.ingresar_valor());
                    int ac = 0;
                    for (int i = 1; i <= suma; i++) {
                        ac = ac + i;
                        
                    }
                    Console.WriteLine($"Sumatorio {ac}");
                    Console.ReadKey();
                    principal.iniciar();
                    break;

                case 3:
                    int factorial = Convert.ToInt32(msf.ingresar_valor());
                    int acfac = 1;
                    for (int j=1; j<=factorial;j++)
                    {
                        acfac = acfac * j;
                    }
                    Console.WriteLine($"Factorial {acfac}");
                    Console.ReadKey();
                    principal.iniciar();
                    break;

            }

        }
        public int ingresar_valor() {
            Validaciones vep = new Validaciones();
            Boolean esNum;
            String valor;
            int numero= 0;
            int nuevo=0;
            do
            {
                Console.WriteLine("Ingrese para su operacion");
                valor = Console.ReadLine();
                esNum = vep.validar(valor);
            } while (!esNum);
            numero = Convert.ToInt32(valor);
            if (numero < 0)
            {
                nuevo = numero * -1;
                Console.WriteLine($"Acaba de ingresar un numero negativo, el sistema lo identifico y \n lo transformara automaticamente en positivo ahora su numero es: {nuevo}");
                return nuevo;
            }
            else {
                
                return numero;
            }
            

        }
        
    }
}

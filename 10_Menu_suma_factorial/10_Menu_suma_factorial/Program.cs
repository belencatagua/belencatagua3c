﻿using System;

namespace _10_Menu_suma_factorial
{
    class Program
    {
        static void Main(string[] args)
        {
            Program msf = new Program();
            msf.iniciar();
            Console.ReadKey();
        }
        public void iniciar() {
            Boolean esNum;
            String valor;
            Validaciones vep = new Validaciones();
            do
            {
                Console.Clear();
                Console.WriteLine("REALIZADO POR BELEN CATAGUA");
                Console.WriteLine("--------------------------");
                Console.WriteLine("Elija la opcion su opcion\n" +
                    "\n************************" +
                    "\n1.- Salir" +
                    "\n2.- Sumatorio" +
                    "\n3.- Factorial" +
                    "\n************************\n");
                Console.WriteLine("Ingrese el numero de una opcion");
                valor = Console.ReadLine();
                esNum = vep.validar(valor);
            } while (!esNum);
            int numero = Convert.ToInt32(valor);
            vep.rango_positivo(numero);

        }
    }
}

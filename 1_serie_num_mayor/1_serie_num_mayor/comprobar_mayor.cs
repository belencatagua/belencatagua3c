﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _1_serie_num_mayor
{
   
   
    class comprobar_mayor
    {

        ///constructor
        public comprobar_mayor()
        {
        }
        //metodo para calcular el numero mayor
        //recibe un parametro 
        double mayor = 0, menor = 0;
        public Double num_mayor(double num,int i) {
            
            if (i == 0)
            {
                mayor = num;
                menor = num;
            }
            else
            {
                if (num > mayor) mayor = num;
                if (num < menor) menor = num;
            }
            return mayor;
        }
    }
}

﻿using System;

namespace _1_serie_num_mayor
{
    class Program
    {
        static void Main(string[] args)
        {
            double mayor = 0;
            int dato;
            double dato1;
            string valor;
            Boolean esNum;
            // "m" me engloba la clase comprobar mayor donde encontramos el metodo para comprobar el numero mayor
            comprobar_mayor m = new comprobar_mayor();
            

            do {
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("Ingrese la Cantidad de numeros que desea comparar");
                Console.ForegroundColor = ConsoleColor.White;
                valor = Console.ReadLine();
                esNum = int.TryParse(valor, out dato);
            } while (!esNum);

            int total = Convert.ToInt32(valor);

            

            for (int i=0; i<total;i++) {

                do {
                    Console.ForegroundColor = ConsoleColor.DarkYellow;
                    Console.WriteLine("ingrese el valor numero {0}", i + 1);
                    Console.ForegroundColor = ConsoleColor.DarkCyan;
                    valor = Console.ReadLine();
                    esNum = Double.TryParse(valor, out dato1);
                } while (!esNum);
                double num = Convert.ToDouble(valor);
                ///guardo en "mayor" lo que me retorna cuando llamo al metodo "num_mayor" que pertenece a "m"
                ///enviando como parametro el numero que ingreso a comprobar con el valor de i
                mayor = m.num_mayor(num, i);
                
            }
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"el Numero Mayo es: {mayor}");
            Console.ReadKey();

        }
    }
}

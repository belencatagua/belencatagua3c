﻿using System;

namespace _4_Salario
{
    class Program
    {
        static void Main(string[] args)
        {
            Calcular_salarios CalSal = new Calcular_salarios();
            Boolean esNum1;
            int valor1;
            String opcion;
            int op = 1;
            do { 
            do
            {
                
                Console.WriteLine("--------------------------");
                Console.WriteLine("Elija la opcion su opcion\n"+
                    "\n************************" +
                    "\n1.- Calcular Salario" +
                    "\n2.- Salir"+
                    "\n************************\n");
                opcion = Console.ReadLine();
                ///int n = Convert.ToInt32(Console.ReadLine());
                esNum1 = int.TryParse(opcion, out valor1);
                ///En la siguietne pregunta vemos la vareable boolean esNum1 que nos ayuda a verificar si el valor String que ingresamos es un entero
                ///o un caracter si es un caracter nos devuelve un valor falso y si es falso no sale del ciclo hasta que se ingrese un valor numerico
                ///dentro de los parametros. esNum1==false viene a ser igual que !esNum1
              
                if (esNum1==false) { Console.Clear(); Console.ForegroundColor = ConsoleColor.Magenta; Console.WriteLine("Solo Numeros porfavor"); Console.ForegroundColor = ConsoleColor.White; }
            } while (!esNum1);
             ///salimos de este ciclo cuando esNum nos devuelva un valor verdader, quiere decir que es numerico, en ese caso devolvemos a op el valor 
             ///que el usuario registro para elegir entre sus opciones
            op = Convert.ToInt32(opcion);
            Console.WriteLine("Usted eligio la opcion {0}",op);
            switch (op) {
                    case 1:
                        ///limpiamos la consola
                        Console.Clear();
                        ///le damos un color al texto que se muestra en consola
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("calcular saldo");
                        ///le devolvemos el color por defecto que tienen las letras por consola
                        Console.ForegroundColor = ConsoleColor.White;
                        ///calcular salario 
                        Console.WriteLine("Ingrese el Nombre del empleado");
                        String nom = Convert.ToString(Console.ReadLine());
                        int valor;
                        string n;
                        bool esNum;
                        ///en este do while especificamos las horas de trabajo, cuenta con la validacion de solo numeros, con el mismo mecanismo
                        ///si esNum retorna un valor falso no selimos del ciclo, en que consiste, en "n" alamacenamos el valor que se le pide al usuario
                        ///sobre las horas, se lo prueba con TRYPARSE donde "valor" guarda 1 o 0 siendo true o false. 
                        do
                        {
                            Console.WriteLine("ingrese las horas trabajadas:");
                            n = Console.ReadLine();
                            esNum = int.TryParse(n, out valor);
                        } while (!esNum);

                        int horas = Convert.ToInt32(n);
                        ///Si horas es un valor negativo lo multiplicamos por -1 para realizar la operacion
                        if (horas < 0)
                        {
                            Console.ForegroundColor = ConsoleColor.DarkYellow;
                            Console.WriteLine("++++++++++++++++++++++++++++++++++++++++++++++");
                            Console.WriteLine("Acaba de ingresar una hora en estado negativo " + horas + ",\nel sistema lo transformara en positivo automaticamente,\nsi desea hacer un nuevo calculo elija la opcion 1");
                            Console.WriteLine("++++++++++++++++++++++++++++++++++++++++++++++");
                            Console.ForegroundColor = ConsoleColor.White;
                            horas = horas * -1;
                        }

                        Double salario = 0;
                        Double extra = 0;
                        ///en este apartado preguntamos entramos a un ciclo como parametro de finalizacion tenemos las horas totales de trabajo,
                        ///representado por "horas",  dentro del ciclo si a es menos que 35 pues esas horas que recurrieron se multilpicaran por 
                        ///15$ que sera el salario sin horas extras, si el usuario pasa de 35 horas "a" no seria menor a 35 por lo que tomara la via
                        ///negativa de la pregunta iniciando una variable acumultiva llamada horas extras. en total hay una variable para las horas laborales normales
                        ///y una variable para las horas extras. al final del bucle se tomas estas dos variables y se ofrece un resultado totalapaga.
                        for (int a = 0; a < horas; a++)
                        {
                            if (a < 35)
                            {

                                salario = CalSal.horas_normal(salario);
                            }
                            else
                            {
                                ///extra = extra + 22;
                                extra = CalSal.horas_extras(extra);
                            }
                        }
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.WriteLine("Valor Normal");
                        Console.WriteLine(salario);
                        Console.ForegroundColor = ConsoleColor.Gray;
                        Console.WriteLine("Valor extra");
                        Console.WriteLine(extra);
                        Console.ForegroundColor = ConsoleColor.Green;
                        Double salariototal = CalSal.sueldo(salario, extra);
                        Console.WriteLine("**********************************************************************");
                        Console.WriteLine($"total a cobrar por: {nom} Sueldo: {salariototal}");
                        Console.WriteLine("**********************************************************************");
                        Console.ForegroundColor = ConsoleColor.White;

                        break;
                    case 2:
                        Console.WriteLine("Cerrando sesión");
                        break;

                

            }
                ///si el valor op que se ingreso al inicio no es un caracter pero si un valor numerico se pregunta fuera de rango se pregunta si ese valor esta fuera del
                ///menu, si es asi op tendra un valor diferente de "2" algo que lo mantiene dentro del blucle y le pedira que ingrese un valor numerico dentro del rango
                if (op != 1 && op != 2)
                {
                    Console.Clear(); Console.ForegroundColor = ConsoleColor.DarkRed; Console.WriteLine("Elija numeros dentro de los parametros"); Console.ForegroundColor = ConsoleColor.White;
                }
            } while (op != 2); ///Sale del ciclo cuando el usuario no desea consultar mas salarios
            Console.WriteLine("pulse enter para salir");
            Console.Read();
        }
    }
}

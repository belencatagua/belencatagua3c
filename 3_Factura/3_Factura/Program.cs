﻿using System;

namespace _3_Factura
{
    class Program
    {
        static void Main(string[] args)
        {
            Detalle_Factura detFac = new Detalle_Factura();
            int cantidad = 0;
            Double precio = 0;
            Double totalapagar = 0;
            String cant;
            String pre;
            int valor;
            Double valor1;
            Boolean esNum;
            int a = 0; ///determinamos la cantidad de productos
            do {
                do
                {
                    Console.WriteLine("Ingrese cantidad de producto {0}", a + 1);
                    cant = Console.ReadLine();
                    ///int n = Convert.ToInt32(Console.ReadLine());
                    esNum = int.TryParse(cant, out valor);
                } while (!esNum);
                cantidad = Convert.ToInt32(cant);
                if (cantidad < 0) {
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine("Acaba de ingresar una cantidad negativa, el sistema resolvera el problema");
                    Console.ForegroundColor = ConsoleColor.White;
                    cantidad = cantidad * -1;
                }

                ///si cantidad en 0 arroja el total hasta ese punto de la venta
                if (cantidad == 0)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("******************");
                    Console.WriteLine("El total a pagar es de");
                    Console.WriteLine(totalapagar);
                    Console.WriteLine("******************");
                    Console.ForegroundColor = ConsoleColor.White;
                    break;
                }
                //ingresamos el precio del producto
                do
                {
                    Console.WriteLine("Ingrese precio del producto {0}", a + 1);
                    pre = Console.ReadLine();
                    ///int n = Convert.ToInt32(Console.ReadLine());
                    esNum = double.TryParse(pre, out valor1);


                } while (!esNum);

                precio = Convert.ToDouble(pre);
                if (precio < 0) {
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine("Acaba de ingresar un precio negativo, el sistema resolvera el problema");
                    Console.ForegroundColor = ConsoleColor.White;
                    precio = precio * -1;
                }

                Double total = detFac.calcular_precio(cantidad, precio);
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine("Valor registrado");
                Console.WriteLine("      {0}", total);
                Console.ForegroundColor = ConsoleColor.White;
                totalapagar = detFac.calcular_total(total);
                a = a + 1;
            } while (cantidad !=0);
            Console.Read();
        }
    }
}

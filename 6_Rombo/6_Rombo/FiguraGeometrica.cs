﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _6_Rombo
{
    class FiguraGeometrica
    {
        public FiguraGeometrica() {
        }

        public void Dibujar_rombo(int alt_) {
            //Console.WriteLine($"dibujar rombo {base_}");
            int bas = (alt_ * 2) - 1;
            int mitad = (alt_ - 1);
            ///tendremos una matriz de alt x bas
            
            Console.WriteLine("Altura: {0}", bas);
            Console.WriteLine("Base: {0}", bas);
            Console.WriteLine("Por Belen Catagua");
            Console.WriteLine("________________________");
            Console.WriteLine("       Solucion       ");
            Console.WriteLine("________________________");
            Console.WriteLine("                        ");

            ///parte superior

            for (int a = 0; a < alt_; a++)
            {
                for (int b = 0; b < bas; b++)
                {

                    if ((a + b) >= mitad && (b - a) <= mitad)
                    {
                        Console.Write(" *");
                    }
                    else
                    {
                        Console.Write("  ");
                    }
                }
                Console.WriteLine("");
            }

            ///parte inferior

            for (int a = alt_ - 2; a >= 0; a--)
            {
                for (int b = 0; b < bas; b++)
                {

                    if ((a + b) >= mitad && (b - a) <= mitad)
                    {
                        Console.Write(" *");
                    }
                    else
                    {
                        Console.Write("  ");
                    }
                }
                Console.WriteLine("");
            }
        }
    }
}

﻿using System;

namespace _8_TresColumnas
{
    class Program
    {
        static void Main(string[] args)
        {

            Program principal = new Program();
            principal.ingresarnum();
            Console.ReadKey();


        }
        public void ingresarnum() {
            Boolean esNum;
            String valor;
            ValidarEnteroPositivo vep = new ValidarEnteroPositivo();
            do
            {
                Console.WriteLine("Ingrese un numero entero positivo");
                valor = Console.ReadLine();
                esNum = vep.validar(valor);
            } while (!esNum);
            int numero = Convert.ToInt32(valor);
            vep.positivo(numero);
            ///Console.WriteLine($"es un numero {numero}");
            
        }
        
    }
}

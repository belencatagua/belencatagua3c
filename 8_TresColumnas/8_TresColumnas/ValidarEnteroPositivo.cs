﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _8_TresColumnas
{
    class ValidarEnteroPositivo
    {
        public ValidarEnteroPositivo() {
        }


        public Boolean validar(String num) {
            int valor;
            Boolean esNum;
            esNum = int.TryParse(num, out valor);
            if (esNum == false) {
                Console.WriteLine("Eso no es un numero entero");
                Console.ReadKey();
                Console.Clear();
            }
            return esNum;
            
        }
        
        public int positivo(int numero) {
            Program principal = new Program();
            Columnas proceso = new Columnas();

            if (numero > 0)
            {
                Console.WriteLine($"se puede trabajar el numero {numero}");
                proceso.resolver(numero);
                
            }
            else {
                Console.WriteLine("es un numero negativo");
                principal.ingresarnum();
                
            }
            return numero;
        }

    }
}

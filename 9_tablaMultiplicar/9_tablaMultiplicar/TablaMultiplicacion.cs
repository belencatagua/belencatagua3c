﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _9_tablaMultiplicar
{
    class TablaMultiplicacion
    {
        public TablaMultiplicacion() {
        }
        public void operaciones(int multiplo) {
            for (int i = 1; i <= 10; i++) {
                Console.WriteLine($"{i}   x   {multiplo}   =   {i * multiplo}");
            }
        }
    }
}

﻿using System;

namespace _5_Piramide
{
    class Program
    {
        static void Main(string[] args)
        {
            FigurasGeometricas fig = new FigurasGeometricas();
            int valor;
            string n;
            bool esNum;
            int alt;
            Console.WriteLine("DATOS DEL PROGRAMA");
            do
            {
                Console.WriteLine("Dame el numero de Altura");
                n = Console.ReadLine();
                ///int n = Convert.ToInt32(Console.ReadLine());
                esNum = int.TryParse(n, out valor);
            } while (!esNum);
            alt = Convert.ToInt32(n);
            int bas = (alt * 2) - 1;
            int mitad = (alt - 1);

            Console.WriteLine("Altura: {0}", alt);
            Console.WriteLine("Base: {0}", bas);
            Console.WriteLine("Por Belen Catagua");
            Console.WriteLine("________________________");
            Console.WriteLine("       Solucion       ");
            Console.WriteLine("________________________");
            Console.WriteLine("                        ");

            fig.piramide(alt, bas, mitad);
            Console.Read();

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _5_Piramide
{
    class FigurasGeometricas
    {
        public FigurasGeometricas()
        {

        }
        public int piramide(int alt, int bas, int mitad) {
            for (int a = 0; a < alt; a++)
            {
                for (int b = 0; b < bas; b++)
                {

                    if ((a + b) >= mitad && (b - a) <= mitad)
                    {
                        Console.Write("*");
                    }
                    else
                    {
                        Console.Write(" ");
                    }
                }
                Console.WriteLine("");
            }
            return 0;
        }
    }
}

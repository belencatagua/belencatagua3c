﻿using System;

namespace _7_Dos_Columna_Numerica
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Columnas de Numeros");
            int desc = 100;
            Console.WriteLine("Ascendente - Descendente");
            for (int asc = 1; asc<= 100; asc++) {
                
                Console.WriteLine($"   {asc}     -     {desc}     ");
                desc = desc- 1;
            }
            Console.ReadLine();
        }
    }
}

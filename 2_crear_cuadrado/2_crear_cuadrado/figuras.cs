﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _2_crear_cuadrado
{
    class Figuras
    {
        public Figuras() {
        }
        public int cuadrado(int lado)
        {
            for (int i = 0; i < lado; i++)
            {
                for (int j = 0; j < lado; j++)
                {
                    if (i == 0 || i == (lado - 1))
                    {
                        Console.Write(" *");
                    }
                    else
                    {
                        if (j == 0 || j == (lado - 1))
                        {
                            Console.Write(" *");
                        }
                        else
                        {
                            Console.Write("  ");
                        }

                    }
                }

                Console.WriteLine();
            }
            return 0;
        }
    }
}

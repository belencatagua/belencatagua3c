﻿using System;

namespace _2_crear_cuadrado
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Figuras fig = new Figuras();
            solonumeros snum = new solonumeros();
            Boolean esNum;
            String valor;
            do {
                Console.WriteLine("Ingrese el valor base del cuadrado");
                valor = Console.ReadLine();
                esNum = snum.validarnumero(valor);
            } while (esNum==false);

            int num = Convert.ToInt32(valor);

            ///Dibujamos el cuadrado llamando a el metodo "cuadrado" que se encuentra en la clase "Figua"
            ///con el siguiente comando

            fig.cuadrado(num);

            Console.ReadKey();
        }
    }
}
